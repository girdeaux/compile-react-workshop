import { createRouter } from 'router5'
import browserPlugin from 'router5-plugin-browser'
import loggerPlugin from 'router5-plugin-logger'
import routes from './routes'

const configureRouter = () => {
  const router = createRouter(routes, { defaultRoute: 'root' })

  router.usePlugin(browserPlugin())
  router.usePlugin(loggerPlugin)

  return router
}

export default configureRouter
