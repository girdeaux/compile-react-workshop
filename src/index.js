// Polyfill first
import '@babel/polyfill'
// Set up global stuff
import './utils/globals'
import './styles/global.scss'

// Rendering
import { render } from 'react-dom'
import App from './components/App'
import './favicon.ico'

// Routing
import { RouterProvider } from 'react-router5'
import { Provider } from 'mobx-react'
import createRouter from './createRouter'
const router = createRouter()

// Stores
import { store } from './stores/stores'

const routeredApp = (
  <RouterProvider router={router}>
    <Provider store={store}>
      <App />
    </Provider>
  </RouterProvider>
)

router.start(() => {
  render(
    routeredApp,
    document.getElementById('app')
  )
})
