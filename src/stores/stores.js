import { observable } from 'mobx'

class GenericStore {
  @observable something = 'this is from store!'
}

const store = new GenericStore()

export {
  store
}
