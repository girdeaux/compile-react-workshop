import * as React from 'react'
// Default luxon to Finnish locale
import { DateTime, Settings } from 'luxon'
Settings.defaultLocale = 'fi-FI'

// Misc
import axios from 'axios'
import * as R from 'ramda'

Object.assign(global, {
  React,
  R,
  axios,
  DateTime
})
