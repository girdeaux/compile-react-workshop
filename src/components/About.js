import { inject, observer } from 'mobx-react'
import { routeNode } from 'react-router5'

@inject('store')
@observer
class About extends React.Component {
  render() {
    const { route, store } = this.props

    return (
      <div>
        about
        <p>{store.something}</p>
        <p>
          {route.name === 'about.sub1' ? 'eka' : undefined}
        </p>
      </div>
    )
  }
}

export default routeNode('about')(About)
