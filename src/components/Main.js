import { routeNode } from 'react-router5'
import Root from './Root'
import About from './About'
import Examples from './Examples'

const Main = ({ route }) => {
  switch (route.name.split('.')[0]) {
    case 'root':
      return <Root />
    case 'about':
      return <About />
    case 'examples':
      return <Examples />
    default:
      return <div>404</div>
  }
}

export default routeNode('')(Main)
