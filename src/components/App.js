import { hot } from 'react-hot-loader/root'
import Nav from './Nav'
import Main from './Main'
// import Styles from './Styles'
import '../styles/global.scss'
import { ThemeContext } from '../context'

// export const ThemeContext = React.createContext('light')

const App = () => (
  <main id="root">
    <ThemeContext.Provider value="blue">
      <Nav />
      <Main />
      {/* <Styles /> */}
    </ThemeContext.Provider>
  </main>
)

export default hot(App)
