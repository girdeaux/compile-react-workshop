import './Styles.scss'

const Styles = () => (
  <div>

    <div styleName="local-test">local</div>
    <div
      className="global-test"
      // styleName="border"
    >
      global
    </div>
  </div>
)

export default Styles
