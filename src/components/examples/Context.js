import { ThemeContext } from '../../context'

const ContextAPI = ({ route }) => (
  <div className="page">
    <h2>Context API</h2>
    Current theme is:&nbsp;
    <ThemeContext.Consumer>
      {value => JSON.stringify(value)}
    </ThemeContext.Consumer>
  </div>
)

export default ContextAPI
