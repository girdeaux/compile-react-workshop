import { useState, lazy, Suspense } from 'react'

const LazyLoaded = lazy(() => import('./LazyLoaded'))

const SubComponent = ({ route }) => {
  const [showLazy, setShowLazy] = useState(false)

  return (
    <div className="page">
      <h2>Suspense with lazy loading</h2>
      <button
        type="button"
        onClick={() => setShowLazy(!showLazy)}
      >
        Toggle show logo
      </button>
      <hr />
      {showLazy && (
        <Suspense fallback={<div>Loading...</div>}>
          <LazyLoaded />
        </Suspense>
      )}
    </div>
  )
}

export default SubComponent
