class Classes extends React.Component {
  constructor() {
    super()
    console.info('constructor')
    this.state = {
      count: 0
    }
  }

  componentDidMount() {
    console.log('Mounted Classes example')
  }

  componentWillUnmount() {
    console.log('Unmounted Classes example')
  }

  render() {
    const { count } = this.state

    return (
      <div className="page">
        <h2>Classes</h2>
        Count: {count}
        <div>
          <button
            type="button"
            onClick={() => this.setState({ count: count + 1 })}
          >
            Increase count
          </button>
          <button
            type="button"
            onClick={() => this.setState({ count: 0 })}
          >
            RESET
          </button>
        </div>
      </div>
    )
  }
}

export default Classes
