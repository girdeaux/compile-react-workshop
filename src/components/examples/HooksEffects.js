import { useState, useEffect } from 'react'

const HooksEffect = ({ route }) => {
  const [count, setCount] = useState(0)

  useEffect(() => {
    console.warn('Effect1 Running on every render')
    document.title = `Clicked count ${count} times`
    // NOTE: no cleanup
  })

  useEffect(() => {
    console.warn('Effect2 Running on every render')
    return () => {
      console.log('CLEANUP after Effect2 (runs BEFORE next render, not straight after the previous)')
    }
  })

  return (
    <div className="page">
      <h2>Hooks with side effects</h2>
      {/* {JSON.stringify(result, null, 2)} */}
      Count: {count}
      <p>(Check console for effects)</p>
      <div>
        <button
          type="button"
          onClick={() => setCount(count + 1)}
        >
          Increase count
        </button>
        <button
          type="button"
          onClick={() => setCount(0)}
        >
          RESET
        </button>
      </div>
    </div>
  )
}

export default HooksEffect
