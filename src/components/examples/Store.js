import { inject, observer } from 'mobx-react'

// Class example here for posterity
@inject('store')
@observer
class Store extends React.Component {
  render() {
    const { store } = this.props

    return (
      <div>
        <p>{store.something}</p>
      </div>
    )
  }
}

// Display store value
const MyText = ({ store }) => {
  return (
    <div>
      {JSON.stringify(store.something)}
    </div>
  )
}
const CMyText = inject('store')(observer(MyText))

// Set store value
const MyInput = ({ store }) => {
  return (
    <div>
      <input
        type="text"
        onChange={e => {
          store.something = e.target.value
        }}
      />
    </div>
  )
}
const CMyInput = inject('store')(observer(MyInput))

const Wrapper = () => (
  <div className="page">
    <h2>MobX store example</h2>
    <CMyInput />
    <hr />
    <CMyText />
  </div>
)

export default Wrapper
