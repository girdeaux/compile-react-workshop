console.warn('Loaded LazyLoaded.js file lazily')

const LazyLoaded = ({ route }) => {
  console.warn('Mounted LazyLoaded-component')

  return (
    <div className="page">
      <img src="https://placekitten.com/300/300" alt="Kitty!" />
    </div>
  )
}

export default LazyLoaded
