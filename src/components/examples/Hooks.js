import { useState } from 'react'

const asyncCall = () => new Promise((resolve, reject) => {
  // Some AJAX GET or something
  setTimeout(() => {
    resolve({
      userId: 1234
    })
  }, 3000)
})

const Hooks = ({ route }) => {
  const [count, setCount] = useState(0)
  const [response, setResponse] = useState('nothing yet')

  const getData = () => asyncCall().then(res => {
    console.info('Got response!', res)
    setResponse(res)
  })

  return (
    <div className="page">
      <h2>Hooks</h2>
      {count}
      <div>
        <button
          type="button"
          onClick={() => setCount(count + 1)}
        >
          Increase count
        </button>
        <button
          type="button"
          onClick={() => setCount(0)}
        >
          RESET
        </button>
      </div>
      <hr />
      <div>
        <button
          type="button"
          onClick={getData}
        >
          Get some fake data
        </button>
        <div>
          {JSON.stringify(response, null, 2)}
        </div>
      </div>
    </div>
  )
}

export default Hooks
