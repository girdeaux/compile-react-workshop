import { useState } from 'react'

const SubComponent = () => {
  const [error, setError] = useState('all fine!')

  if (error === true) {
    throw new Error('Something bad happened :(')
  }
  return (
    <div>
      <h2>Click on the button will trigger ErrorBoundary</h2>
      <button
        type="button"
        onClick={() => {
          setError(true)
        }}
      >
        Throw some error
      </button>
    </div>
  )
}

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error,
      errorInfo
    })
    // You can also log error messages to an error reporting service here
  }

  render() {
    if (this.state.errorInfo) {
      // Error path
      return (
        <div className="page">
          <h2>Something went wrong.</h2>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {this.state.error && this.state.error.toString()}
            <br />
            {this.state.errorInfo.componentStack}
          </details>
        </div>
      );
    }
    // Normally, just render children
    return this.props.children
  }
}
const Wrapper = () => {
  return (
    <ErrorBoundary>
      <SubComponent />
    </ErrorBoundary>
  )
}

export default Wrapper
