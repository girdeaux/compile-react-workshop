import { ConnectedLink } from 'react-router5'

const Nav = () => (
  <nav>
    <ConnectedLink
      routeName="root"
    >Home
    </ConnectedLink>
    <ConnectedLink routeName="examples.classes">Classes</ConnectedLink>
    <ConnectedLink routeName="examples.hooks">Hooks</ConnectedLink>
    <ConnectedLink routeName="examples.hooksEffects">Hooks with side effects</ConnectedLink>
    <ConnectedLink routeName="examples.errorBoundary">ErrorBoundary</ConnectedLink>
    <ConnectedLink routeName="examples.suspense">Suspense</ConnectedLink>
    <ConnectedLink routeName="examples.context">Context</ConnectedLink>
    <ConnectedLink routeName="examples.store">Store</ConnectedLink>
  </nav>
)

export default Nav
