import { routeNode } from 'react-router5'
import Hooks from './examples/Hooks'
import Classes from './examples/Classes'
import HooksEffects from './examples/HooksEffects'
import ErrorBoundary from './examples/Errors'
import Suspense from './examples/Suspense'
import Context from './examples/Context'
import Store from './examples/Store'

const Examples = ({ route }) => {
  // console.log(route, route.name.split('.')[0])
  switch (route.name) {
    case 'examples.hooks':
      return <Hooks />
    case 'examples.classes':
      return <Classes />
    case 'examples.hooksEffects':
      return <HooksEffects />
    case 'examples.errorBoundary':
      return <ErrorBoundary />
    case 'examples.suspense':
      return <Suspense />
    case 'examples.context':
      return <Context />
    case 'examples.store':
      return <Store />
    default:
      return (
        <div className="page">
          <h1>404</h1>
        </div>
      )
  }
}

export default routeNode('examples')(Examples)
