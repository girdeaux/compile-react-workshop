const routes = [
  { name: 'root', path: '/' },

  { name: 'about', path: '/about' },
  { name: 'about.sub1', path: '/sub1' },
  { name: 'about.sub2', path: '/sub2' },

  { name: 'examples', path: '/examples' },
  { name: 'examples.classes', path: '/classes' },
  { name: 'examples.hooks', path: '/hooks' },
  { name: 'examples.hooksEffects', path: '/hooks-effects' },
  { name: 'examples.errorBoundary', path: '/error-boundary' },
  { name: 'examples.suspense', path: '/suspense' },
  { name: 'examples.context', path: '/context' },
  { name: 'examples.store', path: '/store' }
]

export default routes
