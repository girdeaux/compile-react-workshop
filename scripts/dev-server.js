import convert from 'koa-connect'
// Middlewares
import historyApiFallback from 'connect-history-api-fallback'
// import proxy from 'http-proxy-middleware'

import webpack from 'webpack'
import config from '../webpack.config.dev'
import Koa from 'koa'
import koaWebpack from 'koa-webpack'

const serve = async() => {
  const compiler = webpack(config)
  const koaWebpackMiddleware = await koaWebpack({ compiler })

  const app = new Koa()
  app.use(convert(historyApiFallback()))
  app.use(koaWebpackMiddleware)

  app.listen(3000)
}

serve()
