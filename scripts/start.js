const concurrently = require('concurrently')

concurrently([
  'npm:dev',
  'npm:lint:watch'
])
