// Run production build (under ./dist) locally
import Koa from 'koa'
import koaStatic from 'koa-static'

// Middleware
import historyApiFallback from 'connect-history-api-fallback'
// import proxy from 'http-proxy-middleware'
// const proxyTarget = 'http://localhost:8080'

const port = 4000

const app = new Koa()
app.use(koaStatic('./dist'))
app.use(historyApiFallback())
// Proxy to some backend when needed
// app.use(proxy('/api', { target: proxyTarget }))

app.listen(port)

/* eslint-disable-next-line no-console */
console.log(`* PRODUCTION running on http://localhost:${port} *`)
