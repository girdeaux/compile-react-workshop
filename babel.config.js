const config = api => {
  console.log('Babel', api.version, api.env())
  api.cache(true)

  return {
    env: {
      development: {
        presets: [
          '@babel/preset-env',
          '@babel/preset-react'
        ],
        plugins: [
          'react-hot-loader/babel',
          '@babel/plugin-syntax-dynamic-import',
          '@babel/plugin-proposal-optional-chaining',
          [
            '@babel/plugin-proposal-decorators',
            {
              legacy: true
            }
          ],
          [
            '@babel/plugin-proposal-class-properties',
            {
              loose: true
            }
          ],
          [
            'react-css-modules',
            {
              filetypes: {
                '.scss': {
                  syntax: 'postcss-scss'
                }
              }
            }
          ]
        ]
      },
      production: {
        presets: [
          [
            '@babel/preset-env',
            {
              useBuiltIns: 'entry'
            }
          ],
          '@babel/preset-react'
        ],
        plugins: [
          '@babel/plugin-proposal-optional-chaining',
          '@babel/plugin-transform-react-constant-elements',
          [
            'react-css-modules',
            {
              filetypes: {
                '.scss': {
                  syntax: 'postcss-scss'
                }
              }
            }
          ]
        ]
      },
      test: {
        presets: [
          '@babel/preset-env',
          '@babel/preset-react'
        ]
      }
    }
  }
}

module.exports = config
